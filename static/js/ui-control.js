/*Create a single button*/
var singleButton = function() {
    function singleButton({
            name = "", action = "", pin = "", value = "", bgcolor = "#9BDF46", shape = "rectangle",
            align = "center", text_color = "white", add_in_previous = false,
            dualbutton_align = "center", dualbutton_on = false, button_sibling = ""
    }) {
        this.name = name;
        this.action = action;
        this.pin = pin;
        this.value = value;
        this.bgcolor = bgcolor;
        this.shape = shape;
        this.align = align;
        this.text_color = text_color;

        this.outer_div = document.createElement("div");

        this.row_div = document.createElement("div");
        this.row_div.classList.add("row");

        this.first_blank_div = document.createElement("div");
        this.first_blank_div.classList.add("col-md-4", "col-xs-4", "col-sm-4");

        this.second_blank_div = document.createElement("div");
        this.second_blank_div.classList.add("col-md-4", "col-xs-4", "col-sm-4");

        this.div = document.createElement("div");
        this.div.classList.add("col-md-4", "text-center", "col-xs-4", "col-sm-4");
        this.div.style.cssText = "height:100px";

        this.button = document.createElement("BUTTON");
        this.button.classList.add("btn");
        this.button.style.cssText = "background:" + bgcolor + ";color:" + this.text_color + "";

        if (this.shape == "circle") {
            button.classList.add("btn-circle");
        }
        var text = document.createTextNode(this.name.substring(0, 11)); // Create a text node

        this.button.appendChild(text);
        this.div.appendChild(this.button);

        // It will check first if this button need to link with previous button
        if (add_in_previous == false) {

            // Check if the dual button is on, then do the alignment based on dualbutton
            if (dualbutton_on) {
                if (dualbutton_align == "left") {
                    this.row_div.appendChild(this.div);
                    this.row_div.appendChild(this.first_blank_div);
                    this.row_div.appendChild(this.second_blank_div);
                } else if (dualbutton_align == "right") {
                    this.row_div.appendChild(this.first_blank_div);
                    this.row_div.appendChild(this.second_blank_div);
                    this.row_div.appendChild(this.div);
                } else {
                    this.row_div.appendChild(this.first_blank_div);
                    this.row_div.appendChild(this.div);
                    this.row_div.appendChild(this.second_blank_div);
                }
            }
            //else follow the single button alignment
            else {
                if (this.align == "left") {
                    this.row_div.appendChild(this.div);
                    this.row_div.appendChild(this.first_blank_div);
                    this.row_div.appendChild(this.second_blank_div);
                } else if (this.align == "right") {
                    this.row_div.appendChild(this.first_blank_div);
                    this.row_div.appendChild(this.second_blank_div);
                    this.row_div.appendChild(this.div);

                } else {
                    this.row_div.appendChild(this.first_blank_div);
                    this.row_div.appendChild(this.div);
                    this.row_div.appendChild(this.second_blank_div);
                }
            }
        }
        //Apend the new button with previous button
        else {
            button_sibling.insertAdjacentElement('afterend', this.button);
        }

        this.outer_div.appendChild(this.row_div);
        document.body.appendChild(this.outer_div);

        // add on click event
        this.button.onclick = function() {
            if (action == "digitalWrite") {
                digitalWrite(pin, value);
            } else if (action == "analogWrite") {
                analogWrite(pin, value);
            } else {
                console.log("Action is not defined.")
            }
        };
        return this;
    }
    return singleButton;
}();

/*Create a dual button*/
function dualButton(dualbutton_align = "center") {
    var button_sibling;

    function first_button({
            name = "", action = "", pin = "", value = "", bgcolor = "#9BDF46", shape = "rectangle",
            align = "center", text_color = "white"
    }) {
        first_button = singleButton({
            name: name,
            action: action,
            pin: pin,
            value: value,
            bgcolor: bgcolor,
            shape: shape,
            align: align,
            text_color: text_color,
            add_in_previous: false,
            dualbutton_align: dualbutton_align,
            dualbutton_on: true
        })
        button_sibling = first_button.button
        return this;
    }

    function second_button({
            name = "", action = "", pin = "", value = "", bgcolor = "#9BDF46", shape = "rectangle",
            align = "center", text_color = "white", add_in_previous = true
    }) {
        second_button = singleButton({
            name: name,
            action: action,
            pin: pin,
            value: value,
            bgcolor: bgcolor,
            shape: shape,
            align: align,
            text_color: text_color,
            add_in_previous: true,
            dualbutton_align: dualbutton_align,
            dualbutton_on: true,
            button_sibling: button_sibling
        })
        return this;
    }
    return Object.freeze({
        first_button: first_button,
        second_button: second_button
    });
}