/*=====================================================
*
*   _JL : A small library to plot Multiple Graph in Bolt Cloud
*   (c) Inventrom Pvt. Ltd.
    Version: 2.0
    Created: 18/05/2017
    Last Updated: 13/09/2017
*
======================================================*/

/* will store all the instances  of bolt_Graph
============================*/
var bg_instances = [];
var graph_type_lookup_table = {
    'lineGraph': { 'packageName': 'line', 'visualisation_function': 'LineChart', 'options': { 'pointSize': 4 } },
    'barGraph': { 'packageName': 'bar', 'visualisation_function': 'ColumnChart' },
    'scatterGraph': { 'packageName': 'scatter', 'visualisation_function': 'ScatterChart', 'options': { 'pointSize': 4 } },
    'steppedGraph': { 'packageName': 'corechart', 'visualisation_function': 'SteppedAreaChart' },
    'areaGraph': { 'packageName': 'corechart', 'visualisation_function': 'AreaChart' },
    'tableGraph': { 'packageName': 'table', 'visualisation_function': 'Table' }
};

/*  _ Object Constructor
========================*/
function boltGraph() {
    this.graphType = 'tableGraph';
    this.axisNames = [];
    this.fetchDataParameters = [];

    /*default configuration to fetch the data */
    this.duration = "six_hours";
    this.from = ''
    this.to = ''

    this.reset_graph_options();
    /*default configuration to fetch the data */
    this.total_data_counts = 0;

    /*adding random id to chart_id */
    div_id = Math.random().toString(36).substring(7)
    this.div_id= div_id;
    $("#muliple_graph_span").append('<div id='+div_id+'></div>')

    /* Set the required server IP/DNS here */
    this.base_url = window.location.protocol + "//" + window.location.host + "/";

    /* Pushing the this object into array */
    bg_instances.push(this)
}

/*  _ Prototype Functions of boltGraph Clas
============================*/
boltGraph.prototype = {

    reset_graph_options: function(){
        this.graph_options = {
            backgroundColor: { fill:'transparent' },
            chartArea:{left:'5%',top:'10%',width:'90%',height:'70%'},
            isStacked: true,
            hAxis: {
                title: ''
            },
            vAxis: {
                title: ''
            },
            legend: {position:'top',alignment: 'center'},
            width: "90%"
        };
    },

    /* will set the graph type  */
    setChartType: function(ChartType) {
        if (!(ChartType in graph_type_lookup_table)) {
            notify_error("Unknown chart type '" + escapeHTML(ChartType) + "'.<br> Drawing with default Table graph.");
            ChartType = 'tableGraph';
        }
        this.reset_graph_options();
        if ('options' in graph_type_lookup_table[ChartType]) {
            $.extend(this.graph_options,graph_type_lookup_table[ChartType].options);
        }
        this.graphType = ChartType;
        return this;
    },

    /* will return the graph type */
    getChartType: function() {
        graphType = this.graphType;
        return graphType;
    },

    /* will set the AxisNames */
    setAxisName: function (xAxisName, yAxisName) {
        this.graph_options['hAxis']['title'] = xAxisName;
        this.graph_options['vAxis']['title'] = yAxisName;
    },

    /* will return the AxisNames */
    getAxisName: function() {
        return this.axisNames;
    },

    /*
    1. will accept parameters variable
    2. Call a function to load the Google Visualization API
    */
    plotChart: function() {
        arguments_length = arguments.length;
        if(arguments_length<1){
            notify_error('Please check your plotChart() function. No variable names have been passed.');
        }
        $('#graph_section').show();
        this.fetchDataParameters=[];
        for(count = 0; count < arguments_length; count++){
            this.fetchDataParameters.push(arguments[count]);
        }
        this.loadCharts();
        start_reload_timer();
    },

    /* will return  plotchart variables parameters */
    getPlotChartVariables: function() {
        return this.fetchDataParameters;
    },

    /*
    1. will load the Google Visualization API
    2. will call a function custom draw chart
    */
    loadCharts: function() {
        packagesName= this.setPackageGoogleChart(this.getChartType());
        google.charts.load('current',
            { packages: ['corechart', packagesName]
        });
        google.charts.setOnLoadCallback(this.setDataInfo(this));
    },
    /*
    1. will accept chartType as parameter
    2. return the appropirate library name as per google graph guidelines
    */
    setPackageGoogleChart: function(chartType) {
        if(chartType in graph_type_lookup_table){
            return graph_type_lookup_table[chartType].packageName;
        }else{
            return 'table';
        }
    },

    /*
    1. will accept chartObject as parameter
    2. call the setDataPointsInDataObject function to insert the data into chart object
    */

    setDataInfo: function(chartObject){
        return function () {
            chartObject.data = new google.visualization.DataTable();
            chartObject.setAddColumn();
            chartObject.setDataPointsInDataObject(chartObject);
        }
    },

    /* Will add the column type in chart data object */
    setAddColumn: function () {
        if (this.fetchDataParameters[0] == "time_stamp")
            this.data.addColumn('datetime', this.fetchDataParameters[0]);
        else
            this.data.addColumn('number', this.fetchDataParameters[0]);
        for (var i = 1; i < this.fetchDataParameters.length; i++) {
            this.data.addColumn('number', this.fetchDataParameters[i]);
        }
    },

    /*
    1. will accept chartObject as parameter
    2. Will insert row chartObject
    3. call drawGraph function to plot the graph
    */
    setDataPointsInDataObject: function (chartObject) {
        if(this.fetchDataParameters.length<1){
            return;
        }
        url = this.setFetchURL();
        this.fetchDataFromBoltCloud(url).success(function (response) {
            chartObject.parseResponse(response);
        }).fail(function (xhr, exception) {
            get_error_message(xhr, exception);
        });
    },

    /*
    1. will accept chartObject as parameter
    2. Will insert row chartObject
    3. call drawGraph function to plot the graph
    */
    parseResponse: function (response) {
        this.setDataCounts(response.data.length)
        var dataPoints = [];
        var starting_data_point = 0;
        for (var i = starting_data_point; i < response.data.length; i++) {
            var dataPoint = [];
            if (this.fetchDataParameters[0] == 'time_stamp') {
                dataPoint.push(new Date(response.data[i][0]));
            } else {
                dataPoint.push(Number(response.data[i][0]));
            }
            for (var j = 1; j < this.fetchDataParameters.length; j++) {
                dataPoint.push(Number(response.data[i][j]));
            }
            dataPoints.push(dataPoint);
        }
        this.removeData();
        this.data.addRows(dataPoints);
        this.drawGraph();
    },

    /* will set the URL for fetching data of differenet types of graph*/
    setFetchURL: function () {
        fetch_query = 'fetchFromTable?fields=' + this.fetchDataParameters.toString() + '&duration='
            + this.duration + '&deviceName=' + device_name + '&from=' + this.from + '&to=' + this.to + '';
        url = this.base_url + fetch_query;
        return url;
    },

    /* will fetch the data from given URL*/
    fetchDataFromBoltCloud: function(url) {
      return jqxhr =  $.ajax({
                type: 'GET',
                url: url,
                contentType: false,
                processData: false,
                dataType: 'json',
            });
    },

    /* will call the goole draw function to plot the graph*/
    drawGraph: function() {
        div_id = document.getElementById(this.getDivID());
        graphFunction = this.setGraphFunction();
        if (!this.chart) {
            var google_chart_div_object = 'new google.visualization.' + graphFunction + '(div_id)';
            this.chart = eval(google_chart_div_object);
        }
        this.chart.draw(this.data, this.graph_options);
    },

    /* will return element id to plot the graph */
    getDivID: function() {
        return this.div_id;
    },

    /* Will return the google Graph Type Object*/
    setGraphFunction: function() {
        chartType= this.getChartType();
        return graph_type_lookup_table[chartType].visualisation_function;
    },

    /* Will remove the data rows from graph data*/
    removeData: function() {
        var numberofrows= this.data.getNumberOfRows();
        this.data.removeRows(0,numberofrows)
    },

    /* Will remove the data rows from graph data*/
    setDataCounts: function(data_length) {
        $('#data_counts').html(data_length)
    },
    
    /* Will help set the color for the graphs*/
    setGraphOptions: function(option_name, option_value){
        this.graph_options[option_name]=option_value;
    }
};


/*======================================================*/
/* This variable ensures that the reload timer is started only once. */
/*======================================================*/

var reload_timer_started=false;

/*======================================================*/
/* This function will run periodically to update all the graphs */
/*======================================================*/

function start_reload_timer(){
    if(reload_timer_started){
        return;
    }
    reload_timer_started=true;
    setInterval(reload_graphs,18000);
}

/*======================================================*/
/* this function will reload all the graphs */
/*======================================================*/

function reload_graphs() {
    for (index = 0; index < bg_instances.length; index++) {

        bg_instances[index].duration = $('#dayBasedGraph').val();
        if (arguments.length > 1) {
            bg_instances[index].from = arguments[0];
            bg_instances[index].to = arguments[1];
        }

        //will insert the new data points and will plot the graph
        bg_instances[index].setDataPointsInDataObject(bg_instances[index])
        // setTotalDataCounts(bg_instances[index].total_data_counts)
    }
}

/*======================================================*/
/* will handle the data filtering on DropDown Change */
/*======================================================*/
$(document).on('change', '#dayBasedGraph', function (event) {
    reload_graphs('', '');
});

/*======================================================*/
/* will handle the data filtering on date selection */
/*======================================================*/
$(document).on('click', '#filter', function (event) {
    from = $('#from').val();
    to = $('#to').val();
    reload_graphs(from, to);
});


/*======================================================*/
/* download the graph data */
/*======================================================*/
$(document).on('click', '.dataDownload_button', function (event) {
    window.open(download_data_url, 'Download');
});
