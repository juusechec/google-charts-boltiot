(function() {

    var __hs_cta_json = {"css":"a#cta_button_4801849_30e900b1-052b-4c30-b638-c47ea489dd35 {\n    -webkit-font-smoothing: antialiased;\ncursor: pointer;\n-moz-user-select: none;\n-webkit-user-select: none;\n-o-user-select: none;\nuser-select: none;\ndisplay: inline-block;\nfont-weight: normal;\ntext-align: center;\ntext-decoration: none;\n-moz-transition: all .4s ease;\n-webkit-transition: all .4s ease;\n-o-transition: all .4s ease;\nbackground: rgb(155,223,70);\nborder-radius: 6px;\nborder-width: 0px;\ncolor: rgb(255,255,255);\nfont-family: sans-serif;\nheight: auto;\ntransition: all .4s ease;\npadding: 6px 18px;\ntext-shadow: none;\nwidth: auto;\nfont-size: 24px;\nline-height: 1.5em;\n\n    background-color: #9bdf46!important;\nborder: none;\nborder-radius: 2px;\nposition: relative;\nheight: 36px;\nmargin: 0;\nmin-width: 64px;\npadding: 0 16px;\ndisplay: inline-block;\nfont-family: \"Roboto\",\"Helvetica\",\"Arial\",sans-serif;\nfont-size: 14px !important;\nfont-weight: bold;\ntext-transform: uppercase;\nletter-spacing: 0;\noverflow: hidden;\nwill-change: box-shadow;\ntransition: box-shadow .2s cubic-bezier(.4,0,1,1),background-color .2s cubic-bezier(.4,0,.2,1),color .2s cubic-bezier(.4,0,.2,1);\noutline: none;\ncursor: pointer;\ntext-decoration: none;\ntext-align: center;\nline-height: 36px;\nvertical-align: middle;\n  }\n\na#cta_button_4801849_30e900b1-052b-4c30-b638-c47ea489dd35:hover {\nbackground: rgb(170,245,77);\ncolor: rgb(255,255,255);\n}\n\na#cta_button_4801849_30e900b1-052b-4c30-b638-c47ea489dd35:active, #cta_button_4801849_30e900b1-052b-4c30-b638-c47ea489dd35:active:hover {\nbackground: rgb(124,178,56);\ncolor: rgb(244,244,244);\n}\n\n","image_html":"<a id=\"cta_button_4801849_30e900b1-052b-4c30-b638-c47ea489dd35\" class=\"cta_button\" href=\"https://www.boltiot.com/cs/c/?cta_guid=30e900b1-052b-4c30-b638-c47ea489dd35&placement_guid=5cdfb1a9-8520-4008-8bf8-7d3c6fa319d4&portal_id=4801849&canon=https%3A%2F%2Fcloud.boltiot.com%2Fcontrol%3Fname%3DBOLT16394555&redirect_url=APefjpGWRUDbZKiPzFqi-UrNdFCJlBghI2zPSrI19quMdqyz3auObyLARMgkSN3n90LJUPzDcIPxczZqVMwSjCjp_-eq0Z593zT7S1ASlIf76e_GnA--28c&click=748a0e8a-2d62-4cde-bffe-c8bdbe07a896&hsutk=c7a0000010d516ba1fa5016b5109fa24&utm_referrer=https%3A%2F%2Fcloud.boltiot.com%2Fhome%2F\"  target=\"_blank\"  cta_dest_link=\"https://store.boltiot.com\"><img id=\"hs-cta-img-5cdfb1a9-8520-4008-8bf8-7d3c6fa319d4\" class=\"hs-cta-img mdl-button\" style=\"border-width: 0px; /*hs-extra-styles*/\" mce_noresize=\"1\" alt=\"Buy Bolt\" src=\"https://cdn2.hubspot.net/hubshot/18/10/24/5b75b296-f100-4965-9cf0-c3d41e57e798.png\" srcset=\"https://cdn2.hubspot.net/hubshot/18/10/24/979eb498-d738-4085-8de7-410e2e7d42d2.png 4x, https://cdn2.hubspot.net/hubshot/18/10/24/599460c4-6db2-4c76-8102-b685f5bc683f.png 3x, https://cdn2.hubspot.net/hubshot/18/10/24/209b95fd-e57d-486f-9be2-067bde10564f.png 2x\"/></a>","is_image":false,"placement_element_class":"hs-cta-5cdfb1a9-8520-4008-8bf8-7d3c6fa319d4","raw_html":"<a id=\"cta_button_4801849_30e900b1-052b-4c30-b638-c47ea489dd35\" class=\"cta_button mdl-button\" href=\"https://www.boltiot.com/cs/c/?cta_guid=30e900b1-052b-4c30-b638-c47ea489dd35&placement_guid=5cdfb1a9-8520-4008-8bf8-7d3c6fa319d4&portal_id=4801849&canon=https%3A%2F%2Fcloud.boltiot.com%2Fcontrol%3Fname%3DBOLT16394555&redirect_url=APefjpGWRUDbZKiPzFqi-UrNdFCJlBghI2zPSrI19quMdqyz3auObyLARMgkSN3n90LJUPzDcIPxczZqVMwSjCjp_-eq0Z593zT7S1ASlIf76e_GnA--28c&click=748a0e8a-2d62-4cde-bffe-c8bdbe07a896&hsutk=c7a0000010d516ba1fa5016b5109fa24&utm_referrer=https%3A%2F%2Fcloud.boltiot.com%2Fhome%2F\" target=\"_blank\" style=\"/*hs-extra-styles*/\" cta_dest_link=\"https://store.boltiot.com\" title=\"Buy Bolt\"><span style=\"font-size: 14px;\">Buy Bolt</span></a>"};
    var __hs_cta = {};

    __hs_cta.drop = function() {
        var is_legacy = document.getElementById('hs-cta-ie-element') && true || false,
            html = __hs_cta_json.image_html,
            tags = __hs_cta.getTags(),
            is_image = __hs_cta_json.is_image,
            parent, _style;

        if (!is_legacy && !is_image) {
            parent = (document.getElementsByTagName("head")[0]||document.getElementsByTagName("body")[0]);

            _style = document.createElement('style');
            parent.insertBefore(_style, parent.childNodes[0]);
            try {
                default_css = ".hs-cta-wrapper p, .hs-cta-wrapper div { margin: 0; padding: 0; }";
                cta_css = default_css + " " + __hs_cta_json.css;
                // http://blog.coderlab.us/2005/09/22/using-the-innertext-property-with-firefox/
                _style[document.all ? 'innerText' : 'textContent'] = cta_css;
            } catch (e) {
                // addressing an ie9 issue
                _style.styleSheet.cssText = cta_css;
            }

            html = __hs_cta_json.raw_html;

        }
         

        for (var i = 0; i < tags.length; ++i) {

            var tag = tags[i];
            var images = tag.getElementsByTagName('img');
            var cssText = "";
            var align = "";
            for (var j = 0; j < images.length; j++) {
                align = images[j].align;
                images[j].style.border = '';
                images[j].style.display = '';
                cssText = images[j].style.cssText;
            }

            if (align == "right") {
                tag.style.display = "block";
                tag.style.textAlign = "right";
            } else if (align == "middle") {
                tag.style.display = "block";
                tag.style.textAlign = "center";
            }

            tag.innerHTML = html.replace('/*hs-extra-styles*/', cssText);
            tag.style.visibility = 'visible';
            tag.setAttribute('data-hs-drop', 'true');
            window.hbspt && hbspt.cta && hbspt.cta.afterLoad && hbspt.cta.afterLoad('5cdfb1a9-8520-4008-8bf8-7d3c6fa319d4');
        }

        return tags;
    };

    __hs_cta.getTags = function() {
        var allTags, check, i, divTags, spanTags,
            tags = [];
            if (document.getElementsByClassName) {
                allTags = document.getElementsByClassName(__hs_cta_json.placement_element_class);
                check = function(ele) {
                    return (ele.nodeName == 'DIV' || ele.nodeName == 'SPAN') && (!ele.getAttribute('data-hs-drop'));
                };
            } else {
                allTags = [];
                divTags = document.getElementsByTagName("div");
                spanTags = document.getElementsByTagName("span");
                for (i = 0; i < spanTags.length; i++) {
                    allTags.push(spanTags[i]);
                }
                for (i = 0; i < divTags.length; i++) {
                    allTags.push(divTags[i]);
                }

                check = function(ele) {
                    return (ele.className.indexOf(__hs_cta_json.placement_element_class) > -1) && (!ele.getAttribute('data-hs-drop'));
                };
            }
            for (i = 0; i < allTags.length; ++i) {
                if (check(allTags[i])) {
                    tags.push(allTags[i]);
                }
            }
        return tags;
    };

    // need to do a slight timeout so IE has time to react
    setTimeout(__hs_cta.drop, 10);
    
    window._hsq = window._hsq || [];
    window._hsq.push(['trackCtaView', '5cdfb1a9-8520-4008-8bf8-7d3c6fa319d4', '30e900b1-052b-4c30-b638-c47ea489dd35']);
    

}());
