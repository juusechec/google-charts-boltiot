$(document).on('click', '.machine_learning_minus', function() {
    input_value = $(this).siblings('input').val();
    clicked_item_id = $(this).attr('id');
    if (clicked_item_id == 'predicted_points_decrement' && input_value > 1) {
        input_value = input_value - 1;
    }
    if (clicked_item_id == 'no_polynomial_coefficient_decrement' && input_value > 2) {
        console.log('inside')
        input_value = input_value - 1;
    }
    if (clicked_item_id == 'frame_size_decrement' && input_value > 3) {
        input_value = input_value - 1;
    }

    $(this).siblings('input').val(input_value);
});

$(document).on('click', '.machine_learning_plus', function() {
    input_value = $(this).prev('input').val();
    clicked_item_id = $(this).attr('id');
    if (clicked_item_id == 'predicted_points_increment' && input_value < 10) {
        input_value = parseInt(input_value) + 1;
    }
    if (clicked_item_id == 'no_polynomial_coefficient_increment' && input_value < 30) {
        input_value = parseInt(input_value) + 1;
    }
    if (clicked_item_id == 'frame_size_increment' && input_value < 50) {
        input_value = parseInt(input_value) + 1;
    }
    $(this).prev('input').val(input_value);
});


$(document).on('change', '.input_box', function() {
    input_value = $(this).val();
    clicked_item_id = $(this).attr('id');
    if (clicked_item_id == 'predicted_points_input' && (input_value < 1 || input_value > 10)) {
        $("#predicted_points_warning").show();
    }
    setTimeout(function() {
        $("#predicted_points_warning").hide();
    }, 3000);
    if (clicked_item_id == 'no_polynomial_coefficient_input' && (input_value < 2 || input_value > 30)) {
        $("#no_polynomial_coefficient_warning").show();
    }
    setTimeout(function() {
        $("#no_polynomial_coefficient_warning").hide();
    }, 3000);
    if (clicked_item_id == 'frame_size_input' && (input_value < 3 || input_value > 50)) {
        $("#frame_size_warning").show();
    }
    setTimeout(function() {
        $("#frame_size_warning").hide();
    }, 3000);
    $(this).val(input_value);
});